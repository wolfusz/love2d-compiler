@echo off

set pkg=%~dpn1
echo Path: %pkg%

set pkg=%pkg:\=;%
for %%a in (%pkg%) do set name=%%~nxa
echo Testing package: %name%

if "%~x1"==".love" (
 set test="%1"
 goto RUN
)

echo.
echo Creating test package...
set test="D:\Pliki\Programowanie\Lua\LoveMe\test.love"
7z d -tzip "%test%"
7z a -y -r -tzip "%test%" *
echo Done.

:RUN
echo.
echo Starting test...
start /WAIT D:\Pliki\Programowanie\Lua\love-0.9.2-win64\love.exe "%test%" --console

del D:\Pliki\Programowanie\Lua\LoveMe\test.love
echo Love done.

pause