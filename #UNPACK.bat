@echo off

set pkg=%~dpn1
echo Path: %pkg%

set search=%pkg:\=;%
for %%a in (%search%) do set name=%%~nxa
echo Unpacking package: %name%

rd /s /q "%pkg%"
md "%pkg%"

7z x -tzip "%1" -r -o"%pkg%"

echo Love done.