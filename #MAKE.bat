@echo off

set pkg=%~dp1
echo Path: %pkg%

set search=%pkg:\=;%
for /d %%a in (%search%) do set name=%%~nxa
echo Creating .love package: %name%

7z d -tzip "D:\Pliki\Programowanie\Lua\proj\%name%.love"
7z a -y -r -tzip "D:\Pliki\Programowanie\Lua\proj\%name%.love" "%pkg%\*"

echo Love done.