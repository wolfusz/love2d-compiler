@echo off

set pkg=%~dpn1
echo Path: %pkg%

set search=%pkg:\=;%
for %%a in (%search%) do set name=%%~nxa
echo Creating distribution: %name%

set pkg="D:\Pliki\Programowanie\Lua\LoveMe\#DIST\%name%"

rd /s /q "%pkg%"
md "%pkg%"
copy "D:\Pliki\Programowanie\Lua\love-0.9.2-win64\*.dll" "%pkg%"

copy /B "D:\Pliki\Programowanie\Lua\love-0.9.2-win64\love.exe"+"%1" "%pkg%\%name%.exe"

echo Done.
echo.
echo Press '1' to open folder
echo Press '2' to run exe
echo Press '3' or wait 5 seconds to exit
CHOICE /C 123 /D 3 /N /T 5

if "%ERRORLEVEL%"=="1" explorer "%pkg%"
if "%ERRORLEVEL%"=="2" "%pkg%\%name%.exe"

echo Love done.
